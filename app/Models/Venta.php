<?php

namespace App\Models;

use App\Model;
use App\Models\User;
use App\Models\Cliente;
use App\Models\Producto;
use App\Models\MetodoPago;

class Venta extends Model
{
    protected $fillable = ['descripcion'];

    protected $appends = [
        'importe_cobro',
        'importe_pagado',
        'importe_restante',
    ];

    public function usuario()
    {
        return $this->belongsTo(User::class, 'usuario_id');
    }

    public function cliente()
    {
        return $this->belongsTo(Cliente::class);
    }

    public function productos()
    {
        return $this->belongsToMany(Producto::class)
            ->withPivot('cantidad')->withTimestamps();
    }

    public function pagos()
    {
        return $this->hasMany(MetodoPago::class, 'venta_id');
    }

    public function getImporteCobroAttribute()
    {
        return $this->productos->reduce(function ($carry, Producto $producto) {
            return $carry + (($producto->importe + $producto->cantidad_iva) * $producto->pivot->cantidad);
        });
    }

    public function getImportePagadoAttribute()
    {
        return $this->pagos->reduce(function ($carry, MetodoPago $pago) {
            return $carry + $pago->cantidad;
        });
    }

    public function getImporteRestanteAttribute()
    {
        return $this->importe_pagado - $this->importe_cobro;
    }
}
