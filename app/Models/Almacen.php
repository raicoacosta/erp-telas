<?php

namespace App\Models;

use App\Model;

use App\Models\EntradaAlmacen;

class Almacen extends Model
{
    protected $table = 'almacenes';

    protected $fillable = ['nombre'];

    public function entradas()
    {
        return $this->hasMany(EntradaAlmacen::class);
    }
}
