<?php

namespace App\Models;

use App\Model;
use App\Models\Marca;
use App\Models\Venta;
use App\Models\Producto;
use App\Models\Categoria;
use App\Models\Cotizacion;
use App\Models\EntradaAlmacen;
use Illuminate\Database\Eloquent\Builder;

class Producto extends Model
{
    protected $fillable = [
        'nombre',
        'descripcion',
        'sku',
        'iva',
        'ieps',
        'costo',
        'is_paquete',
        'multiplicador',
        'peso',
        'dimensiones',
        'color',
    ];

    protected $casts = [
        'is_paquete' => 'boolean',
        'iva' => 'boolean',
        'ieps' => 'real',
        'costo' => 'real',
        'multiplicador' => 'real',
    ];

    protected $appends = [
        'precio',
        'importe',
        'cantidad_iva',
    ];

    public function getPrecioAttribute()
    {
        $precio = $this->costo * $this->multiplicador;
        $iva = $precio * config('vat.iva');
        if ($this->ieps > 0) {
            $ieps = $precio * ($this->ieps / 100);
            $precio += $ieps;
        }

        if ($this->iva) {
            $precio += $iva;
        }

        return $precio;
    }

    public function getImporteAttribute()
    {
        $precio = $this->costo * $this->multiplicador;

        return $precio;
    }

    public function getCantidadIvaAttribute()
    {
        $precio = $this->costo * $this->multiplicador;
        $iva = $precio * config('vat.iva');

        return $iva;
    }

    public function paquetes()
    {
        return $this->belongsToMany(Producto::class, 'paquete_producto', 'producto_id', 'paquete_id')
        ->withPivot('cantidad')->withTimestamps();
    }

    public function productos()
    {
        return $this->belongsToMany(Producto::class, 'paquete_producto', 'paquete_id', 'producto_id')
        ->withPivot('cantidad')->withTimestamps();
    }

    public function marca()
    {
        return $this->belongsTo(Marca::class);
    }

    public function categoria()
    {
        return $this->belongsTo(Categoria::class);
    }

    public function entradas()
    {
        return $this->hasMany(EntradaAlmacen::class);
    }

    public function cotizaciones()
    {
        return $this->belongsToMany(Cotizacion::class)->withPivot('cantidad')->withTimestamps();
    }

    public function ventas()
    {
        return $this->belongsToMany(Venta::class)->withPivot('cantidad')->withTimestamps();
    }

    public function scopeInAlmacen(Builder $query, $almacen)
    {
        return $query->whereHas('entradas', function (Builder $query) use ($almacen) {
            $query->where('almacen_id', $almacen);
        });
    }

    public function getExistencias($almacen)
    {
        $this->load('entradas');
        return $this->entradas->reduce(function ($carry, $existencia) use ($almacen) {
            if ($existencia->almacen_id == $almacen) {
                return $carry + $existencia->cantidad;
            } else {
                return $carry + 0;
            }
        });
    }
}
