<?php

namespace App\Models;

use App\Model;
use App\Models\ClienteContacto;
use App\Models\ClienteDatosEnvio;
use App\Models\ClienteDatosFiscales;
use App\Models\Credito;

class Cliente extends Model
{
    protected $fillable = ['nombre', 'can_creditos'];

    protected $casts = [
        'can_creditos' => 'boolean'
    ];

    /**
     * Relacion para ClienteDatosFiscales
     *
     * @return \Eloquent\Relations\HasMany
     */
    public function datosFiscales()
    {
        return $this->hasMany(ClienteDatosFiscales::class);
    }

    /**
     * Relacion para ClienteContacto
     *
     * @return \Eloquent\Relations\HasMany
     */

    public function contactos()
    {
        return $this->hasMany(ClienteContacto::class);
    }

    /**
     * Relacion para ClienteDatosEnvio
     *
     * @return \Eloquent\Relations\HasMany
     */
    public function datosEnvio()
    {
        return $this->hasMany(ClienteDatosEnvio::class);
    }

    /**
     * Relacion para Credito
     *
     * @return \Eloquent\Relations\HasMany
     */
    public function creditos()
    {
        return $this->hasMany(Credito::class);
    }
}
