<?php

namespace App\Models;

use App\Model;

class ClienteDatosEnvio extends Model
{
    protected $fillable = [
        'nombre',
        'calle',
        'numero_interior',
        'numero_exterior',
        'colonia',
        'municipio',
        'codigo_postal',
        'estado',
        'pais',
    ];

    public function cliente()
    {
        $this->belongsTo(Cliente::class);
    }
}
