<?php

namespace App\Models;

use App\Model;

class ClienteDatosFiscales extends Model
{
    protected $fillable = [
        'tipo_persona',
        'razon_social',
        'rfc',
        'calle',
        'colonia',
        'numero_interior',
        'numero_exterior',
        'municipio',
        'estado',
        'codigo_postal',
        'pais',
        'referencia',
    ];

    public function cliente()
    {
        return $this->belongsTo(Cliente::class);
    }
}
