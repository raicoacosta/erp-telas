<?php

namespace App\Models;

use App\Model;
use App\Models\ProveedorContacto;
use App\Models\ProveedorDireccion;

class Proveedor extends Model
{
    protected $table = 'proveedores';

    protected $fillable = ['nombre'];

    public function contactos()
    {
        return $this->hasMany(ProveedorContacto::class);
    }

    public function direcciones()
    {
        return $this->hasMany(ProveedorDireccion::class);
    }
}
