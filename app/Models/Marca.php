<?php

namespace App\Models;

use App\Model;

class Marca extends Model
{
    protected $fillable = ['nombre'];
}
