<?php

namespace App\Models;

use App\Model;
use App\Models\Cliente;
use App\Models\MetodoPago;

class Credito extends Model
{
    const PENDIENTE = 'Pendiente';
    const PAGADO = 'Pagado';

    protected $fillable = ['cantidad', 'status'];

    public function getIsPagadoAttribute()
    {
        return $this->status === self::PAGADO;
    }

    public function getIsPendienteAttribute()
    {
        return $this->status === self::PENDIENTE;
    }

    public function cliente()
    {
        return $this->belongsTo(Cliente::class);
    }

    public function pago()
    {
        return $this->hasOne(MetodoPago::class, 'credito_id');
    }
}
