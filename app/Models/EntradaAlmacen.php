<?php

namespace App\Models;

use App\Model;
use App\Models\Almacen;
use App\Models\Producto;

class EntradaAlmacen extends Model
{
    protected $table = 'entrada_almacenes';

    protected $fillable = [
        'cantidad',
        'serie',
        'lote',
    ];

    public function producto()
    {
        return $this->belongsTo(Producto::class);
    }

    public function almacen()
    {
        return $this->belongsTo(Almacen::class);
    }
}
