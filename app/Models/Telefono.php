<?php

namespace App\Models;

use App\Model;

class Telefono extends Model
{
    protected $fillable = [
        'codigo',
        'tipo',
        'numero',
        'extension'
    ];

    public function telefonable()
    {
        return $this->morphTo();
    }
}
