<?php

namespace App\Models;

use App\Model;
use App\Models\Telefono;
use App\Models\Proveedor;
use Carbon\Carbon;

class ProveedorContacto extends Model
{
    protected $fillable = [
        'nombre',
        'apellidos',
        'email',
        'email_secundario',
        'tipo',
        'puesto',
        'fecha_nacimiento',
    ];

    protected $dates = ['deleted_at', 'fecha_nacimiento'];

    public function setFechaNacimientoAttribute($value)
    {
        if ($value instanceof \DateTime) {
            $this->attributes['fecha_nacimiento'] = $value;
        } else {
            $date = Carbon::parse($value);
            $this->attributes['fecha_nacimiento'] = $date;
        }
    }

    public function proveedor()
    {
        return $this->belongsTo(Proveedor::class);
    }

    public function telefonos()
    {
        return $this->morphMany(Telefono::class, 'telefonable');
    }
}
