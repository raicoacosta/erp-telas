<?php

namespace App\Models;

use App\Model;

use App\Models\Cliente;
use App\Models\Producto;

class Cotizacion extends Model
{
    protected $table = 'cotizaciones';

    protected $fillable = ['descripcion'];

    protected $appends = ['importe'];

    public function cliente()
    {
        return $this->belongsTo(Cliente::class);
    }

    public function productos()
    {
        return $this->belongsToMany(Producto::class)->withPivot('cantidad')->withTimestamps();
    }

    public function getImporteAttribute()
    {
        return $this->productos->reduce(function ($carry, Producto $producto) {
            return $carry + (($producto->importe + $producto->cantidad_iva) * $producto->pivot->cantidad);
        });
    }
}
