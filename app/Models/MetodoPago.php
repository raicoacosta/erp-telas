<?php

namespace App\Models;

use App\Model;
use App\Models\Venta;
use App\Models\Credito;

class MetodoPago extends Model
{
    const EFECTIVO = 'Efectivo';
    const TARJETA = 'Tarjeta';
    const CHEQUE = 'Cheque';
    const CREDITO = 'Crédito';
    const TRANSFERENCIA = 'Tranferencia';

    protected $fillable = [
        'tipo',
        'observaciones',
        'cantidad',
    ];

    protected $casts = [
        'observaciones' => 'object'
    ];

    public function venta()
    {
        return $this->belongsTo(Venta::class);
    }

    public function credito()
    {
        return $this->belongsTo(Credito::class);
    }
}
