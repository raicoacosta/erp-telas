<?php

namespace App\Models;

use App\Model;
use App\Models\ConceptoGasto;

class Gasto extends Model
{
    protected $fillable = [
        'tipo',
        'recurrencia',
        'cantidad',
        'archivo',
    ];

    public function concepto()
    {
        return $this->belongsTo(ConceptoGasto::class);
    }
}
