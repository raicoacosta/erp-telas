<?php

namespace App\Models;

use App\Model;
use App\Models\Telefono;
use Carbon\Carbon;

class ClienteContacto extends Model
{
    protected $fillable = [
        'nombre',
        'apellidos',
        'email',
        'email_secundario',
        'tipo',
        'puesto',
        'fecha_nacimiento',
    ];

    protected $dates = ['deleted_at', 'fecha_nacimiento'];

    public function cliente()
    {
        $this->belongsTo(Cliente::class);
    }

    public function telefonos()
    {
        return $this->morphMany(Telefono::class, 'telefonable');
    }

    public function setFechaNacimientoAttribute($value)
    {
        if ($value instanceof \DateTime) {
            $this->attributes['fecha_nacimiento'] = $value;
        } else {
            $fecha_nacimiento = Carbon::parse($value);
            $this->attributes['fecha_nacimiento'] = $fecha_nacimiento;
        }
    }
}
