<?php

namespace App\Models;

use App\Model;

class Categoria extends Model
{
    protected $fillable = ['nombre'];
}
