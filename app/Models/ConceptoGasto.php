<?php

namespace App\Models;

use App\Model;
use App\Models\Gasto;

class ConceptoGasto extends Model
{
    protected $fillable = ['nombre'];

    public function gastos()
    {
        return $this->hasMany(Gasto::class);
    }
}
