<?php

namespace App\Models;

use App\Model;
use App\Models\Proveedor;

class ProveedorDireccion extends Model
{
    protected $table = 'proveedor_direcciones';

    protected $fillable = [
        'nombre',
        'calle',
        'numero_interior',
        'numero_exterior',
        'colonia',
        'municipio',
        'codigo_postal',
        'estado',
        'pais',
    ];

    public function proveedor()
    {
        return $this->belongsTo(Proveedor::class);
    }
}
