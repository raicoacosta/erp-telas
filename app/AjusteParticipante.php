<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use App\Evento;
use App\Participante;
use App\RenglonEvento;
use App\TipoParticipanteEvento;

class AjusteParticipante extends Model
{
	protected $table ="ajuste_participante";
	
    protected $fillable = [
    					'participante_id',
                        'tipo_evento',
                        'ajuste_id'
					];
	public function participante(){
		return $this->belongsTo(Participante::class,'participante_id','id');
	}
    public function tipo(){
        return $this->belongsTo(TiposdeParticipantes::class,'tipo_participante_id','id');
    }
    public function particip($evento){
        return $this->where('evento_id',$evento)->get();
    }
    public function renglon(){
        return $this->belongsTo(RenglonEvento::class,'renglon_evento_id','id');
    }
}
