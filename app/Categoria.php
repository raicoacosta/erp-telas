<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use App\Evento;

class Categoria extends Model
{
	protected $table ="categoria";
	
    //protected $fillable = ['nombre','edad_minima','edad_maxima'];
    protected $fillable = ['nombre'];
    public function mayusculas($palabra){
    	return strtoupper($palabra);
    }
}
