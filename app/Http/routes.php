<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the controller to call when that URI is requested.
|
*/

Route::get('/', function () {
    return view('welcome');
});

Route::group(['middleware' => 'auth'], function () {
	Route::group(['prefix' => 'catalogo'], function () {
	    get('gastos','GastosController@index')->name('catalogo.gastos.index');
		get('gastos/create','GastosController@create')->name('catalogo.gastos.create');
		post('gastos','GastosController@store')->name('catalogo.gastos.store');
		get('gastos/{id}','GastosController@show')->name('catalogo.gastos.show');
		get('gastos/{id}/edit','GastosController@edit')->name('catalogo.gastos.edit');
		put('gastos/{id}','GastosController@update')->name('catalogo.gastos.update');
		delete('gastos/{id}','GastosController@destroy')->name('catalogo.gastos.destroy');
	});
    /*Route::get('user', 'AuthController@user');
    Route::resource('users', 'UsersController');
    Route::resource('clientes', 'ClientesController');
    Route::resource('clientes.contactos', 'ClienteContactosController');
    Route::resource('clientes.contactos.telefonos', 'ContactoTelefonosController');
    Route::resource('clientes.fiscales', 'ClienteDatosFiscalesController');
    Route::resource('clientes.envios', 'ClienteDatosEnviosController');
    Route::resource('proveedores', 'ProveedoresController');
    Route::resource('proveedores.contactos', 'ProveedoresContactosController');
    Route::resource('proveedores.contactos.telefonos', 'ProveedoresTelefonosController');
    Route::resource('proveedores.direcciones', 'ProveedoresDireccionesController');
    Route::resource('categorias', 'CategoriasController');
    Route::resource('conceptos', 'ConceptosGastosController');
    Route::resource('marcas', 'MarcasController');
    Route::get('productos/{producto}/existencias', 'ProductosController@existencias');
    Route::get('productos/search', 'ProductosController@search');
    Route::resource('productos', 'ProductosController');
    Route::resource('almacenes', 'AlmacenesController');
    Route::resource('almacenes.productos', 'AlmacenesProductosController', ['only' => ['index', 'show']]);
    Route::resource('entradas-almacen', 'EntradasAlmacenController', ['only' => 'store']);
    Route::resource('gastos', 'GastosController');
    Route::resource('cotizaciones', 'CotizacionesController');
    Route::resource('creditos', 'CreditosController');
    Route::resource('ventas', 'VentasController');)*/
});
