<!-- Left side column. contains the logo and sidebar -->
<aside class="main-sidebar">

    <!-- sidebar: style can be found in sidebar.less -->
    <section class="sidebar">

        <!-- Sidebar user panel (optional) -->
        <div class="user-panel">
            <div class="pull-left image">
                <img src="{{asset('/img/user2-160x160.jpg')}}" class="img-circle" alt="User Image" />
            </div>
            <div class="pull-left info">
                <p>{{ Auth::user()->name }}</p>
                <!-- Status -->
                <a href="#"><i class="fa fa-circle text-success"></i> Online</a>
            </div>
        </div>

        <!-- Sidebar Menu -->
        <ul class="sidebar-menu">
            <li class="header">Menu</li>
            <!-- Optionally, you can add icons to the links -->
            <li class="active"><a href="{{ url('home') }}"><i class='fa fa-tachometer'></i> <span>Dashboard</span></a></li>
            <li><a href="#"><i class='fa fa-archive'></i> <span>Almacenes</span></a></li>
            <li><a href="#"><i class='fa fa-book'></i> <span>Clientes</span></a></li>
            <li><a href="#"><i class='fa fa-credit-card'></i> <span>Créditos</span></a></li>
            <li><a href="#"><i class='fa fa-copy'></i> <span>Cotizaciones</span></a></li>
            <li><a href="#"><i class='fa fa-link'></i> <span>Gastos</span></a></li>
            <li><a href="#"><i class='fa fa-money'></i> <span>Ventas</span></a></li>
            <li><a href="#"><i class='fa fa-bookmark'></i> <span>Proveedores</span></a></li>
            <li class="treeview">
                <a href="#"><i class='fa fa-cogs'></i> <span>Catalogos</span> <i class="fa fa-angle-left pull-right"></i></a>
                <ul class="treeview-menu">
                    <li><a href="#">Categorias</a></li>
                    <li><a href="{{route('catalogo.gastos.index')}}">Conceptos de Gastos</a></li>
                    <li><a href="#">Marcas</a></li>
                    <li><a href="#">Productos</a></li>
                </ul>
            </li>
            <li><a href="#"><i class='fa fa-user'></i> <span>Usuarios</span></a></li>
        </ul><!-- /.sidebar-menu -->
    </section>
    <!-- /.sidebar -->
</aside>
