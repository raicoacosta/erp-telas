<?php

use Illuminate\Database\Seeder;

use App\Models\Producto;

class ProductosTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Producto::truncate();
        factory(Producto::class, 30)->make()->each(function (Producto $producto) {
            $producto->marca()->associate(rand(1, 10));
            $producto->categoria()->associate(rand(1, 10));
            $producto->save();
        });
    }
}
