<?php

use Illuminate\Database\Seeder;
use App\Models\Telefono;
use App\Models\Proveedor;
use App\Models\ProveedorContacto;
use App\Models\ProveedorDireccion;

class ProveedoresTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Proveedor::truncate();
        ProveedorContacto::truncate();
        ProveedorDireccion::truncate();
        factory(Proveedor::class, 10)->create()->each(function (Proveedor $proveedor) {
            $proveedor->contactos()->saveMany(factory(ProveedorContacto::class, 2)->make())
                ->each(function (ProveedorContacto $contacto) {
                    $contacto->telefonos()->saveMany(factory(Telefono::class, 2)->make());
                });
            $proveedor->direcciones()->saveMany(factory(ProveedorDireccion::class, 2)->make());
        });
    }
}
