<?php

use Illuminate\Database\Seeder;

use App\Models\Categoria;

class CategoriasTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Categoria::truncate();
        factory(Categoria::class, 10)->create();
    }
}
