<?php

use Illuminate\Database\Seeder;

use App\Models\Almacen;

class AlmacenTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Almacen::truncate();

        factory(Almacen::class, 3)->create();
    }
}
