<?php

use Illuminate\Database\Seeder;

use App\Models\Marca;

class MarcasTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Marca::truncate();
        factory(Marca::class, 10)->create();
    }
}
