<?php

use Illuminate\Database\Seeder;
use App\Models\Cliente;
use App\Models\Telefono;
use App\Models\ClienteContacto;
use App\Models\ClienteDatosEnvio;
use App\Models\ClienteDatosFiscales;

class ClientesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Cliente::truncate();
        ClienteDatosEnvio::truncate();
        ClienteDatosFiscales::truncate();
        ClienteContacto::truncate();

        $cliente = Cliente::create([
            'nombre' => 'PÚBLICO EN GENERAL'
        ]);
        $cliente->datosFiscales()->create([
            'rfc' => 'XAXX010101000',
            'razon_social' => 'VENTAS A PÚBLICO EN GENERAL',
            'tipo_persona' => 'Persona Física',
            'calle' => '',
            'numero_exterior' => '',
            'colonia' => '',
            'municipio' => '',
            'codigo_postal' => '',
            'estado' => '',
            'pais' => 'México',
        ]);

        factory(Cliente::class, 10)->create()->each(function ($cliente) {
          $cliente->datosFiscales()->saveMany(factory(ClienteDatosFiscales::class, 2)->make());
          $cliente->datosEnvio()->saveMany(factory(ClienteDatosEnvio::class, 2)->make());
          $cliente->contactos()->saveMany(factory(ClienteContacto::class, 2)->create()->each(function ($contacto) {
            $contacto->telefonos()->saveMany(factory(Telefono::class, 2)->make());
          }));
        });
    }
}
