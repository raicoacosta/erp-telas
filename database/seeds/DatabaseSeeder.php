<?php

use Illuminate\Database\Seeder;
use Illuminate\Database\Eloquent\Model;

use App\Models\Telefono;

class DatabaseSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::statement('SET FOREIGN_KEY_CHECKS = 0');

        Model::unguard();
        Telefono::truncate();
        $this->call(UsersTableSeeder::class);
        $this->call(ClientesTableSeeder::class);
        $this->call(ProveedoresTableSeeder::class);
        $this->call(CategoriasTableSeeder::class);
        $this->call(MarcasTableSeeder::class);
        $this->call(ProductosTableSeeder::class);
        $this->call(AlmacenTableSeeder::class);
        $this->call(ConceptoGastoTableSeeder::class);
        Model::reguard();

        DB::statement('SET FOREIGN_KEY_CHECKS = 1');
    }
}
