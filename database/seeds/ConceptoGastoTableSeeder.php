<?php

use Illuminate\Database\Seeder;

use App\Models\Gasto;
use App\Models\ConceptoGasto;

class ConceptoGastoTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        ConceptoGasto::truncate();
        Gasto::truncate();

        factory(ConceptoGasto::class, 10)->create();
    }
}
