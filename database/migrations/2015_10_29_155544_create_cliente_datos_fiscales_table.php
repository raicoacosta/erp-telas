<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateClienteDatosFiscalesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('cliente_datos_fiscales', function (Blueprint $table) {
            $table->engine = 'InnoDB';
            $table->increments('id');
            $table->string('tipo_persona');
            $table->string('razon_social');
            $table->string('rfc');
            $table->string('calle');
            $table->string('numero_exterior');
            $table->string('numero_interior')->nullable();
            $table->string('colonia');
            $table->string('municipio');
            $table->string('estado');
            $table->string('codigo_postal');
            $table->string('pais');
            $table->string('referencia')->nullable();

            $table->integer('cliente_id')->unsigned();

            $table->timestamps();
            $table->softDeletes();

            $table->foreign('cliente_id')->references('id')
                ->on('clientes')->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('cliente_datos_fiscales');
    }
}
