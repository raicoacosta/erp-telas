<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateMetodoPagosTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('metodo_pagos', function (Blueprint $table) {
            $table->increments('id');
            $table->string('tipo');
            $table->text('observaciones')->nullable('');
            $table->decimal('cantidad', 21, 2)->default(1);
            $table->unsignedInteger('venta_id');
            $table->unsignedInteger('credito_id')->nullable();
            $table->timestamps();
            $table->softDeletes();
            $table->foreign('venta_id')->references('id')
                ->on('ventas');
            $table->foreign('credito_id')->references('id')
                ->on('creditos');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('metodo_pagos');
    }
}
