<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateEntradaAlmacensTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('entrada_almacenes', function (Blueprint $table) {
            $table->increments('id');
            $table->decimal('cantidad', 18, 2);
            $table->string('serie');
            $table->string('lote');
            $table->unsignedInteger('producto_id');
            $table->unsignedInteger('almacen_id');
            $table->timestamps();
            $table->softDeletes();

            $table->foreign('producto_id')->references('id')
                ->on('productos');
            $table->foreign('almacen_id')->references('id')
                ->on('almacenes');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('entrada_almacenes');
    }
}
