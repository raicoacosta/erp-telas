<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatePaqueteProductoPivotTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('paquete_producto', function (Blueprint $table) {
            $table->unsignedInteger('paquete_id');
            $table->unsignedInteger('producto_id');
            $table->decimal('cantidad', 15, 3);
            $table->timestamps();
            $table->foreign('paquete_id')->references('id')
                ->on('productos');
            $table->foreign('producto_id')->references('id')
                ->on('productos');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('paquete_producto');
    }
}
