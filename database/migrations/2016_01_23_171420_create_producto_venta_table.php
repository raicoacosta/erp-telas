<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateProductoVentaTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('producto_venta', function (Blueprint $table) {
            $table->unsignedInteger('producto_id');
            $table->unsignedInteger('venta_id');
            $table->bigInteger('cantidad');
            $table->timestamps();

            $table->foreign('producto_id')->references('id')
                ->on('productos');
            $table->foreign('venta_id')->references('id')
                ->on('ventas');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('producto_venta');
    }
}
