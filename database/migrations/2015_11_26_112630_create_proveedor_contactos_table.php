<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateProveedorContactosTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('proveedor_contactos', function (Blueprint $table) {
            $table->increments('id');
            $table->string('nombre');
            $table->string('apellidos');
            $table->string('email');
            $table->string('email_secundario')->nullable();
            $table->string('tipo');
            $table->string('puesto');
            $table->date('fecha_nacimiento')->nullable();
            $table->unsignedInteger('proveedor_id');
            $table->timestamps();
            $table->softDeletes();
            $table->foreign('proveedor_id')->references('id')
                ->on('proveedores')->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('proveedor_contactos');
    }
}
