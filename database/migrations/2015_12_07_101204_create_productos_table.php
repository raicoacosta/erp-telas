<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateProductosTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('productos', function (Blueprint $table) {
            $table->increments('id');
            $table->string('nombre');
            $table->string('descripcion')->nullable();
            $table->string('sku');
            $table->boolean('iva')->default(false);
            $table->boolean('ieps')->default(false);
            $table->decimal('costo', 15, 2);
            $table->decimal('multiplicador', 6, 2)->default(1);
            $table->string('peso')->nullable();
            $table->string('dimensiones')->nullable();
            $table->string('color')->nullable();
            $table->unsignedInteger('marca_id');
            $table->unsignedInteger('categoria_id');
            $table->timestamps();
            $table->softDeletes();

            $table->foreign('categoria_id')->references('id')
                ->on('categorias');
            $table->foreign('marca_id')->references('id')
                ->on('marcas');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('productos');
    }
}
