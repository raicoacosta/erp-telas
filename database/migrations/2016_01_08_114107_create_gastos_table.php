<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateGastosTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('gastos', function (Blueprint $table) {
            $table->increments('id');
            $table->string('tipo');
            $table->string('recurrencia')->nullable();
            $table->decimal('cantidad', 21, 2);
            $table->string('archivo')->nullable();
            $table->unsignedInteger('concepto_id');
            $table->timestamps();
            $table->softDeletes();

            $table->foreign('concepto_id')->references('id')
                ->on('concepto_gastos');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('gastos');
    }
}
