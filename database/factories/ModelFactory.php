<?php

/*
|--------------------------------------------------------------------------
| Model Factories
|--------------------------------------------------------------------------
|
| Here you may define all of your model factories. Model factories give
| you a convenient way to create models for testing and seeding your
| database. Just tell the factory how a default model should look.
|
*/

// $faker->addProvider(new Faker\Provider\es_ES\PhoneNumber($faker));
// $faker->addProvider(new Faker\Provider\es_ES\Internet($faker));
// $faker->addProvider(new Faker\Provider\es_ES\Person($faker));
// $faker->addProvider(new Faker\Provider\es_ES\Company($faker));
// $faker->addProvider(new Faker\Provider\es_ES\Address($faker));

$factory->define(App\Models\User::class, function (Faker\Generator $faker) {
    return [
        'name' => $faker->name,
        'email' => $faker->safeEmail,
        'password' => bcrypt('control'),
    ];
});

$factory->define(App\Models\Cliente::class, function (Faker\Generator $faker) {
    return [
        'nombre' => $faker->name
    ];
});

$factory->define(App\Models\Almacen::class, function (Faker\Generator $faker) {
    return [
        'nombre' => ucfirst($faker->word(10))
    ];
});

$factory->define(App\Models\ClienteContacto::class, function (Faker\Generator $faker) {
    return [
        'nombre' => $faker->firstName,
        'apellidos' => $faker->lastName,
        'email' => $faker->safeEmail,
        'email_secundario' => $faker->safeEmail,
        'puesto' => $faker->word(4),
        'tipo' => $faker->randomElement(['Principal', 'Cobranza', 'Secundario']),
        'fecha_nacimiento' => $faker->dateTime,
    ];
});

$factory->define(App\Models\ClienteDatosEnvio::class, function (Faker\Generator $faker) {
    return [
        'nombre' => $faker->name,
        'calle' => $faker->streetName,
        'numero_interior' => $faker->buildingNumber,
        'numero_exterior' => $faker->buildingNumber,
        'colonia' => $faker->secondaryAddress,
        'municipio' => $faker->city,
        'codigo_postal' => $faker->postcode,
        'estado' => $faker->state,
        'pais' => 'Mexico',
    ];
});

$factory->define(App\Models\ClienteDatosFiscales::class, function (Faker\Generator $faker) {
    return [
        'tipo_persona' => $faker->randomElement(['Persona Física', 'Persona Moral']),
        'razon_social' => $faker->company.' '.$faker->companySuffix,
        'rfc' => $faker->regexify('[A-Z,Ñ,&]{3,4}[0-9]{2}[0-1][0-9][0-3][0-9][A-Z,0-9]?[A-Z,0-9]?[0-9,A-Z]?'),
        'calle' => $faker->streetName,
        'numero_interior' => $faker->buildingNumber,
        'numero_exterior' => $faker->buildingNumber,
        'colonia' => $faker->secondaryAddress,
        'municipio' => $faker->city,
        'codigo_postal' => $faker->postcode,
        'estado' => $faker->state,
        'pais' => 'México',
        'referencia' => $faker->sentence,
    ];
});

$factory->define(App\Models\Telefono::class, function (Faker\Generator $faker) {
    return [
        'numero' => $faker->phoneNumber,
        'tipo' => $faker->randomElement(['Casa', 'Oficina', 'Celular']),
        'extension' => $faker->optional(0.4)->randomNumber(4),
    ];
});

$factory->define(App\Models\Proveedor::class, function (Faker\Generator $faker) {
    return [
        'nombre' => $faker->name
    ];
});

$factory->define(App\Models\ProveedorContacto::class, function (Faker\Generator $faker) {
    return [
        'nombre' => $faker->firstName,
        'apellidos' => $faker->lastName,
        'email' => $faker->safeEmail,
        'email_secundario' => $faker->safeEmail,
        'puesto' => $faker->word(4),
        'tipo' => $faker->randomElement(['Principal', 'Cobranza', 'Secundario']),
        'fecha_nacimiento' => $faker->dateTime,
    ];
});

$factory->define(App\Models\ProveedorDireccion::class, function (Faker\Generator $faker) {
    return [
        'nombre' => $faker->name,
        'calle' => $faker->streetName,
        'numero_interior' => $faker->buildingNumber,
        'numero_exterior' => $faker->buildingNumber,
        'colonia' => $faker->secondaryAddress,
        'municipio' => $faker->city,
        'codigo_postal' => $faker->postcode,
        'estado' => $faker->state,
        'pais' => 'Mexico',
    ];
});

$factory->define(App\Models\Categoria::class, function (Faker\Generator $faker) {
    return [
        'nombre' => $faker->word(6)
    ];
});

$factory->define(App\Models\Marca::class, function (Faker\Generator $faker) {
    return [
        'nombre' => $faker->word(6)
    ];
});

$factory->define(App\Models\ConceptoGasto::class, function (Faker\Generator $faker) {
    return [
        'nombre' => $faker->word(6)
    ];
});

$factory->define(App\Models\Producto::class, function (Faker\Generator $faker) {
    return [
        'nombre' => $faker->word(6),
        'descripcion' => $faker->sentence(3),
        'sku' => $faker->randomNumber(5),
        'iva' => $faker->randomElement([true, false]),
        'ieps' => $faker->randomFloat(2, 0.1, 2),
        'costo' => $faker->randomFloat(2, 1, 1000),
        'multiplicador' => $faker->randomFloat(2, 1, 10),
        'peso' => $faker->word(6),
        'dimensiones' => $faker->word(6),
        'color' => $faker->word(6),
    ];
});

$factory->define(App\Models\EntradaAlmacen::class, function (Faker\Generator $faker) {
    return [
        'cantidad' => $faker->randomFloat(2, 1, 1000),
        'serie' => $faker->isbn10,
        'lote' => $faker->isbn13,
    ];
});

$factory->define(App\Models\Gasto::class, function (Faker\Generator $faker) {
    return [
        'tipo' => $faker->randomElement(['1 vez', 'Recurrente']),
        'recurrencia' => $faker->randomElement(['Semanal', 'Mensual', 'Anual']),
        'cantidad' => $faker->randomFloat(2, 1, 99999),
        'archivo' => $faker->imageUrl(800, 400)
    ];
});

$factory->define(App\Models\Credito::class, function (Faker\Generator $faker) {
    return [
        'cantidad' => $faker->randomFloat(2, 1, 99999),
    ];
});
